#!/bin/bash

export PROJECTPATH=$PWD
export GOPATH="$PROJECTPATH/go"
export GO111MODULE=off

if [ -z "$1" ]; then
	echo "Usage: ./bind.sh <gmitohtml commit hash>"
	exit 1
fi

echo "Downloading gomobile..."
GOPATH="$PROJECTPATH/go" go get -u golang.org/x/mobile/cmd/...

echo "Downloading gmitohtml $1..."
mkdir -p $GOPATH/src/gitlab.com/tslocum
if [ ! -d "$GOPATH/src/gitlab.com/tslocum/gmitohtml" ]; then
	git clone https://gitlab.com/tslocum/gmitohtml $GOPATH/src/gitlab.com/tslocum/gmitohtml
fi

cd $GOPATH/src/gitlab.com/tslocum/gmitohtml && git fetch origin && git reset --hard `printf %q "$1"` && cd $PROJECTPATH

#echo "Downloading dependencies..."
#cd $GOPATH/src/gitlab.com/tslocum/gmitohtml
#go mod vendor
#cd $PROJECTPATH

echo "Binding library..."
mkdir -p libs
PATH="$PROJECTPATH/go/bin:$PATH"
$GOPATH/bin/gomobile bind -javapkg space.rocketnine -o libs/gmitohtml.aar -target=android -androidapi=16 $GOPATH/src/gitlab.com/tslocum/gmitohtml/pkg/gmitohtml

echo "Finished."
